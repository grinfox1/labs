package sample;

import java.util.Scanner;

public class Calc2 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        double a, b;
        try {
            System.out.print("a=");
            a = sc.nextDouble();
            System.out.println("Enter the operation (+ - / or *):");
            String oper = sc.next();
            System.out.print("b=");
            b = sc.nextDouble();


            switch (oper) {
                case "+":
                    System.out.println(a + b);
                    break;
                case "-":
                    System.out.println(a - b);
                    break;
                case "*":
                    System.out.println(a * b);
                    break;
                case "/":
                    System.out.println(a / b);
                    break;
                default:
                    System.out.println("No such operation");
                    break;
            }
            }   catch(Exception e){
                System.err.println("Not a number");
            }
        }

    }
