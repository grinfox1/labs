package sample;

import java.util.Scanner;

public class Calc3 {

    static void vivod(double c){
        int f= (int)c;
        if(f==c)
            System.out.println(f);
        else
            System.out.println(String.format("%.1f", c));
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        double a, b, c;

        try {
            System.out.print("a=");
            a = sc.nextDouble();
            System.out.println("Enter the operation (+ - / or *):");
            String oper = sc.next();
            System.out.print("b=");
            b = sc.nextDouble();

            switch (oper) {
                case "+":
                    c=a + b; vivod(c);
                    break;
                case "-":
                    c=a - b; vivod(c);
                    break;
                case "*":
                    c=a * b; vivod(c);
                    break;
                case "/":
                    c=a / b; vivod(c);
                    break;
                default:
                    System.out.println("No such operation");
                    break;
            }
        }   catch(Exception e){
            System.err.println("Not a number");
        }
    }

}
