package sample;

import java.util.Scanner;

public class Calc {

    public static void main(String[] args) {

        Scanner sc =new Scanner(System.in);
        double a;
        System.out.print("a=");
        a=sc.nextInt();

        System.out.print("Enter the operation (+ - / or *):");
        
        String oper = sc.next();

        double b;
        System.out.print("b=");
        b=sc.nextInt();

        if (oper.equals("+"))
            System.out.println("Your Answer is: " + (a+b));

        if (oper.equals("-"))
            System.out.println("Your Answer is: "+ (a-b));

        if (oper.equals("/"))
            System.out.println("Your Answer is: "+ (a/b));

        if (oper.equals("*"))
            System.out.println("Your Answer: "+ (a*b));

    }

}