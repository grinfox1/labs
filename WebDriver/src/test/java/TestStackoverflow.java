import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.time.*;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hao 5/31/2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    private static String url;


    @BeforeClass
    public static void getWebDriver(){
        driver = new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        //driver.manage().window().maximize();
    }

    @Test
    public void counts(){
        WebElement cou = driver.findElement(By.xpath(".//*[@id='tabs']/a[2]/span"));
        String d = cou.getText();
        int s = Integer.parseInt(d);
        boolean moreThan = false;
        if(s>300){
            moreThan=true;
        }
        Assert.assertTrue(moreThan);
    }


    @Test
    public void facebook(){
        WebElement signup = driver.findElement(By.xpath(".//a[text()='sign up']"));
        signup.click();
        WebElement google = driver.findElement(By.xpath("//*[text()='Google']"));
        Assert.assertTrue(google.isDisplayed());
        WebElement facebook = driver.findElement(By.xpath("//*[text()='Facebook']"));
        Assert.assertTrue(facebook.isDisplayed());

    }

    @Test
    public void question()throws Exception{
        WebElement firstQ = driver.findElement(By.xpath(".//div[@id='question-mini-list']/div[1]/div[2]/h3/a"));
        firstQ.click();
        WebElement date = driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr[1]/td[2]/p"));
        String temp = date.getAttribute("title");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'Z'");
        Date dateFromStack = format.parse(temp);
        Date currentDate = new Date();
        Assert.assertTrue(dateFromStack.getDate()==currentDate.getDate());
    }

    @Test
    public void money()
    {
        boolean mon=false;
        try
        {
            WebElement moneyOffer = driver.findElement(By.xpath(".//*[@id='hireme']//*[@class=\"salary\"]"));
            String temp = moneyOffer.getText();
            //String temp = "£40,000 - £60,000";
            String[] splited = temp.split(" - ");
            String lastValueMoney = splited[1].replaceAll("£", "");
            lastValueMoney=lastValueMoney.replaceAll(",", "");
            lastValueMoney=lastValueMoney.replaceAll(" ", "");
            //System.out.println(lastValueMoney);
            //int money = Integer.parseInt(lastValueMoney);

            try{
                int money = Integer.parseInt(lastValueMoney);
                if(money>100000)
                {
                    mon=true;
                }
            }
            catch (Exception e)
            {
                mon=false;
            }
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }
        catch (Exception e){
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }

    }

    @AfterClass
    public static void setDown(){
        driver.quit();
    }
}
