import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by hao on 5/31/2016.
 */
public class TestRozetka {
    private static WebDriver driver;
    private static String url;

    @BeforeClass
    public static void getWebDriver() throws Exception
     {
        driver = new FirefoxDriver();
        //driver.manage().window().maximize();
        driver.get("http://rozetka.com.ua/");
       // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void logoRozetka(){
        WebElement logoRozetka = driver.findElement(By.xpath(".//*[@id='body-header']/div[2]/div/div[2]/div[1]/img"));
        assertNotNull("Rozetka is not found", logoRozetka);

    }

    @Test
    public void appleItem(){
        WebElement appleItem = driver.findElement(By.xpath(".//*[@href='http://rozetka.com.ua/apple/c4627486/'][@level='level1']"));
        Assert.assertTrue("Apple logo is not found", appleItem.isDisplayed());
    }

    @Test
    public void MP3(){
        WebElement mp3 = driver.findElement(By.xpath(".//*[@menu_name='main_menu']/li/a[contains(text(),'MP3')]"));
        Assert.assertTrue(mp3.isDisplayed());
    }


    @Test
    public void city(){
        WebElement city = driver.findElement(By.xpath(".//*[@id='city-chooser']"));
        city.click();
        WebElement cityKharkov=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Харьков')]"));
        Assert.assertTrue(cityKharkov.isDisplayed());
        WebElement cityOdessa=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Одесса')]"));
        Assert.assertTrue(cityOdessa.isDisplayed());
        WebElement cityKiev=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Киев')]"));
        Assert.assertTrue(cityKiev.isDisplayed());

    }

    @Test
    public void cart(){
        WebElement cart = driver.findElement(By.xpath(".//*[@href='https://my.rozetka.com.ua/cart/']//*[contains(text(), 'Корзина')]"));
        cart.click();
        WebElement emptyCart = driver.findElement(By.xpath(".//*[@class='empty-cart-title inline sprite-side'][contains(text(),'Корзина пуста')]"));
        Assert.assertTrue(emptyCart.isDisplayed());
        WebElement close = driver.findElement(By.xpath(".//*[@id='cart-popup']/a/img"));
        close.click();
    }

    @AfterClass
    public static void setDown() throws Exception {
        driver.quit();
    }


}
