1.
a) .//*[@id='header']


b)
.//*[@class='bounty-indicator-tab']

c)
.//*[@class='question-hyperlink']

d)
.//*[contains(@id, 'question-summary')]//*[contains(text(), 'How')]

e) by 'bash' keyword
.//*[text()='bash']/../..//*[@class='question-hyperlink']

2.
.//img[contains(@alt, 'Selenium') OR contains(@alt, 'Logo')]

3.
a)
.//*[contains(@class, 'locked')]
b)
.//*[contains(text(), 'Sticky')]/..
c)
.//xhtml:tr[@class='stickied-topic']//xhtml:a[@class='topic-title']

4 

.//*[@class='navFooterLine navFooterLinkLine navFooterPadItemLine ']/ul/li[position()>5]

.//*[@class='navFooterLine navFooterLinkLine navFooterPadItemLine ']/ul/li[position()>5]/a
