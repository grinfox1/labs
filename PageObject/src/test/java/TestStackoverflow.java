import StackOverflow.FirstQuestion;
import StackOverflow.Main;
import StackOverflow.QuestionSummary;
import StackOverflow.SignUp;
import org.junit.Assert;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


/**
 * Created by hao on 6/17/16.
 */

public class TestStackoverflow {
    private static WebDriver driver;
    protected static Main main;
    protected static QuestionSummary questionSummary;
    protected static SignUp signUp;
    protected static FirstQuestion firstQuestion;


        @BeforeClass
    public static void getWebDriver(){
        driver=new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        //driver.manage().window().maximize();
        main=new Main(driver);
    }

    @AfterClass
    public static void close(){
        driver.close();
    }

    @Test
    public void checkFeaturedCounts() throws Exception{
        int count=Integer.valueOf(main.featuredTabCount.getText());
        Assert.assertTrue("Features count is less than 300",count>300);
    }

    @Test
    public void checkGoogleFacebook() throws Exception{
        signUp=main.openSignUpPage();
        Assert.assertTrue("Google button is not found",signUp.googleButton.isDisplayed());
        Assert.assertTrue("Facebook button is not found",signUp.facebookButton.isDisplayed());
    }

    @Test
    public void question()throws Exception{
        firstQuestion=Main.openfirstQuestion();
        String temp = firstQuestion.date.getAttribute("title");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'Z'");
        Date dateFromStack = format.parse(temp);
        Date currentDate = new Date();
        Assert.assertTrue(dateFromStack.getDate()==currentDate.getDate());

        firstQuestion.returnToMain();

    }

    @Test
    public void money()
    {
        boolean mon=false;
        try
        {
            WebElement moneyOffer = driver.findElement(By.xpath(".//*[@id='hireme']//*[@class=\"salary\"]"));
            String temp = moneyOffer.getText();
            String[] splited = temp.split(" - ");
            String lastValueMoney = splited[1].replaceAll("£", "");
            lastValueMoney=lastValueMoney.replaceAll(",", "");
            lastValueMoney=lastValueMoney.replaceAll(" ", "");

            try{
                int money = Integer.parseInt(lastValueMoney);
                if(money>100000)
                {
                    mon=true;
                }
            }
            catch (Exception e)
            {
                mon=false;
            }
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }
        catch (Exception e){
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }

    }

}
