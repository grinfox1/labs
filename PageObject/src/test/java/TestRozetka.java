import Rozetka.Cart;
import Rozetka.MainPage;
import org.junit.Assert;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by hao on 6/17/16.
 */

public class TestRozetka {
    private static WebDriver driver;
    protected static MainPage mainPage;
    protected static Cart cart;

    @BeforeClass
    public static void GetWebDriver(){
        driver=new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
       // driver.manage().window().maximize();
        mainPage=new MainPage(driver);

    }

    @AfterClass
    public static void after(){
        driver.close();
    }

    @Test
    public void checkLogoIsDisplayed()throws Exception{
        Assert.assertTrue("Can't find the banner",mainPage.logo.isDisplayed());
    }

    @Test
    public void checkMenuContainsAppleItem() throws Exception{
        Assert.assertTrue("Can't find link Apple in the menu",mainPage.appleItemFromMenu.isDisplayed());
    }

    @Test
    public void checkMenuContainsMP3Item() throws Exception{
        Assert.assertTrue("Can't find the section in the menu with MP3",mainPage.itemWithMP3FromMenu.isDisplayed());
    }

    @Test
    public void checkCity() throws Exception{

        mainPage.cities.click();
        Assert.assertTrue("Can not find  Харьков-city",mainPage.kharkivCity.isDisplayed());
        Assert.assertTrue("Can not find  Киев-city",mainPage.kievCity.isDisplayed());
        Assert.assertTrue("Can not find  Одесса-city",mainPage.odessaCity.isDisplayed());

    }

    @Test
    public void checkCartIsEmpty() throws Exception{
        cart=mainPage.openCart();
        Assert.assertTrue("Cart is not empty",cart.emptyCart.isDisplayed());
    }
}
