package StackOverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by hao on 6/17/16.
 */
public class SignUp {
    public static WebDriver driver;

    public SignUp(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath=".//span[text()='Google']")
    public WebElement googleButton;

    @FindBy(xpath=".//span[text()='Facebook']")
    public WebElement facebookButton;

}
