package StackOverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


/**
 * Created by hao on 6/17/16.
 */

public class Main {
    public static WebDriver driver;

    public Main(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath=".//*[@id='tabs']/a/span")
    public WebElement featuredTabCount;


    @FindBy(xpath = ".//div[@id='question-mini-list']/div[1]/div[2]/h3/a")
    public static WebElement firstQ;

    public static FirstQuestion openfirstQuestion(){
        firstQ.click();
        return new FirstQuestion(driver);
    }

    @FindBy(xpath = ".//*[@id='hireme']//*[@class='salary']")
    public WebElement moneyOffer;

    @FindBy(xpath=".//*[@class='topbar-menu-links']/a[contains(text(),'sign up')]")
    public WebElement signUpButton;

    public SignUp openSignUpPage(){
        signUpButton.click();
        return new SignUp(driver);
    }
}
