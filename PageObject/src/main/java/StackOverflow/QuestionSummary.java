package StackOverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by hao on 6/17/16.
 */
public class QuestionSummary {
    public static WebDriver driver;

    public QuestionSummary(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath=".//*[@id='qinfo']/tbody/tr/td/p/b")
    public WebElement askedText;

    @FindBy(xpath = ".//*[@id='hireme']//*[@class='salary']")
    public WebElement moneyOffer;
}
