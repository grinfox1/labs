package StackOverflow;

/**
 * Created by hao on 6/17/2016.
 */

import org.junit.rules.Timeout;
import org.openqa.selenium.*;
import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class FirstQuestion {
    private WebDriver driver;

    public FirstQuestion(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@id='qinfo']/tbody/tr[1]/td[2]/p")
    public WebElement date;

    @FindBy(xpath = ".//*[@id='hlogo']/a")
    public WebElement logo;



    public Main returnToMain (){
        logo.click();
        return new Main(driver);

    }

}
