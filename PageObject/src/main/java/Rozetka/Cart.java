
package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by hao on 6/17/16.
 */
public class Cart {

    public WebDriver driver;

    public Cart (WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath=".//*[@class='wrap-cart-empty']")
    public WebElement emptyCart;
}
